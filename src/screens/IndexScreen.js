import React,{ useContext } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Button,
  TouchableOpacity
} from 'react-native';
import { Context as BlogContext } from '../context/BlogContext'
import Icon from 'react-native-ionicons'

const IndexScreen = ({navigation}) => {
    const {state, addBlogPost, deleteBlogPost} = useContext(BlogContext);
    return (
        <View>
            <FlatList
                data={state}
                keyExtractor={(blogPost)=>blogPost.id}
                renderItem={({item})=>{
                return <TouchableOpacity onPress={()=> navigation.navigate('Show',{id:item.id})}><View style={styles.row}>
                        <Text style={styles.title}>{item.title}-{item.id}</Text>
                        <TouchableOpacity onPress={()=>deleteBlogPost(item.id)}>
                            <Icon style={styles.icon} name="trash" />
                        </TouchableOpacity>
                    </View></TouchableOpacity>
                }}
            />
        </View>
    )
};

IndexScreen.navigationOptions = ({navigation}) => {
    return {
        headerRight: () => (
          <TouchableOpacity onPress={() => navigation.navigate('Create')}>
            <Icon style={styles.icon} name="add" />
          </TouchableOpacity>
        ),
      };
};

const styles = StyleSheet.create({
    row:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingVertical: 20,
        borderTopWidth: 1,
        borderColor: 'gray',
        paddingHorizontal: 10
    },
    title:{
        fontSize: 18
    },
    icon: {
        fontSize: 24,
        marginRight: 10
    }
})

export default IndexScreen;
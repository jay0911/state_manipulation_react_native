import React,{useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';
import {Context as BlogContext} from '../context/BlogContext'
import Icon from 'react-native-ionicons'

const ShowScreen = ({navigation}) => {
    const{state} = useContext(BlogContext);
    
    const blogPost = state.find((blogPost)=>blogPost.id === navigation.getParam('id'))

    return (
    <View>
        <Text>{blogPost.title}</Text>
        <Text>{blogPost.content}</Text>
    </View>
    )
};

ShowScreen.navigationOptions = ({navigation}) => {
    return {
        headerRight: () => (
          <TouchableOpacity onPress={() => navigation.navigate('Edit', {id:navigation.getParam('id')})}>
            <Icon style={styles.icon} name="document" />
          </TouchableOpacity>
        ),
      };
};

const styles = StyleSheet.create({
    icon: {
        fontSize: 24,
        marginRight: 10
    }
})

export default ShowScreen;
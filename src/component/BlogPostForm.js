import React,{useContext, useState} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button
} from 'react-native';

const BlogPostForm = ({onSubmit, initialValue}) => {
    const[title,setTitle] = useState(initialValue.title);
    const[content,setContent] = useState(initialValue.content);
 
    return (
    <View>
        <Text style={styles.label}>Enter Title:</Text>
        <TextInput style={styles.textInput} value={title}
        onChangeText={(text=>setTitle(text))}/>

        <Text style={styles.label}>Enter Content:</Text>
        <TextInput style={styles.textInput} value={content}
        onChangeText={(content=>setContent(content))}/>
        
        <Button 
            title="Save Blog Post"
            onPress={()=>onSubmit(title,content)} />
    </View>
    )
};

BlogPostForm.defaultProps = {
    initialValue: {
        title: '',
        content: ''
    }
}

const styles = StyleSheet.create({
    textInput:{
        borderColor:'black',
        borderWidth:1,
        fontSize:18,
        marginBottom: 15,
        padding: 5,
        margin: 5
    },
    label: {
        fontSize:20,
        marginBottom:10
    }
})

export default BlogPostForm;